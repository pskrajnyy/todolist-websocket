const express = require('express');
const socket = require('socket.io');
const path = require('path');

const app = express();
const port = process.env.PORT || 8000;

const tasks = [];

app.use(express.static(path.join(__dirname, '/client/build')));

app.get('*', (req, res) => {
  res.sendFile(path.join(__dirname, '/client/build/index.html'));
});

app.use((req, res) => {
  res.status(404).send({ message: 'Not found...' });
});

const server = app.listen(port, () => console.log(`Server start on port ${port}`));
const io = socket(server);

io.on('connection', (socket) => {
  socket.emit('updateData', tasks);
  
  socket.on('addTask', (task) => {
    tasks.push(task);
    socket.broadcast.emit('addTask', task);
  });

  socket.on('removeTask', (id) => {
    tasks.splice(tasks.findIndex(task => task.id === id), 1);
    socket.broadcast.emit('removeTask', id);
  });

  socket.on('editTask', (newName, id) => {
    tasks[tasks.findIndex(task => task.id === id)].name = newName;
    socket.broadcast.emit('editTask', newName, id);
  });
});